public class StringPracticeDemo {
	public static void main(String[] args) {
		StringPractice demo = new StringPractice();
		String name1 = "Saranga";
		String name2 = "Vanaja";
		String name3 = "Sharuja";
		String name4 = "Mathusa";
		String name5 = "   Varmila   ";

		demo.CharAt(name1);
		demo.Concat(name1, name2);
		demo.IndexOf1(name1);
		demo.IndexOf2(name1);
		demo.IndexOf3(name1, name2);
		demo.IndexOf4(name3, name4);
		demo.LastIndexOf1(name2);
		demo.LastIndexOf2(name2);
		demo.LastIndexOf3(name3, name4);
		demo.LastIndexOf4(name2, name1);
		demo.Length(name2);
		demo.Replace(name2);
		demo.StartsWith(name1);
		demo.Substring1(name1);
		demo.Substring2(name1);
		demo.ToLowerCase(name1);
		demo.ToUpperCase(name2);
		demo.Trim(name5);
	}
   }